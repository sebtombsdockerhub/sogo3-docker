# SOGo 3 nightly build

This repository generates a docker image for the latest nightly-build of [SoGo](https://sogo.nu) version 3


## PLEASE NOTE that whilst the Dockerfile builds successfully the result is currently completely untested !!!!

* The docker image generated is suitable for testing of SOGo
* It is *not* recommended for production use as it is based on the latest nightly build, not a stable release
* The SOGo version is 3

## Setup

The image stores configuration, logs and backups in `/srv`, which you should persist somewhere. Example configuration is copied during each startup of the container, which you should adjust for your own use. For creating the initial directory hierarchy and example configuration, simply run the container with the `/srv` volume already exposed or linked, for example using

    docker run -v /srv/sogo:/srv sebtombs/sogo3

As soon as the files are created, stop the image again. You will now find following file tree:

    .
    ├── etc
    │   ├── apache-SOGo.conf.orig
    │   └── sogo.conf.orig
    └── lib
        └── sogo
            └── GNUstep
                ├── Defaults
                └── Library

The configuration files named `apache-SOGo.conf.orig` and `sogo.conf.orig` are initial versions. Copy these without the suffix. Don't change or link the `.orig` files, as they will be overwritten each time the container is started. They can be used to see differences in configuration after SOGo upgrades.

### Database

A separate database is required, for example a PostgreSQL container as provided by the Docker image [postgres:alpine](https://hub.docker.com/_/postgres/), but also any other database management system SOGo supports can be used.
The database and user needs creating before SOGo will work. Please refer to the _Database Configuration_ section of the [Installation Guide](https://sogo.nu/files/docs/SOGo%20Installation%20Guide.pdf) on how to do this and then modify the sogo.conf` file accordingly. The following documentation will expect the database to be available with the SOGo default credentials given by the official documentation, adjust them as needed. If you link a database container, remember that it will be automatically added to the hosts file and be available under the chosen name.

For a container named `sogo-postgresql` linked as `db` using `--link="sogo-postgresql:db"` with default credentials, you would use following lines in the `sogo.conf`:

    SOGoProfileURL = "postgresql://sogo:sogo@db:5432/sogo/sogo_user_profile";
    OCSFolderInfoURL = "postgresql://sogo:sogo@db:5432/sogo/sogo_folder_info";
    OCSSessionsFolderURL = "postgresql://sogo:sogo@db:5432/sogo/sogo_sessions_folder";

SOGo performs schema initialziation lazily on startup, thus no database initialization scripts need to be run.

### User authentication

You will need to set up one of the user authentication options as explained in the [Installation Guide](https://sogo.nu/files/docs/SOGo%20Installation%20Guide.pdf). For initial testing, using the database may be appropriate.

### memcached

As most users will not want to separate memcached, there is a built-in daemon. It can be controled by setting the environment variable `memcached`. If set to `false`, the built-in memcached will not start, make sure to configure an external one. Otherwise, the variable holds the amount of memory dedicated to memcached in MiB. If unset, a default of 64MiB will be used.

### Sending Mail

For convenience reasons, the gateway is added to the hostsfile as host `GATEWAY` before starting the SOGo daemon. This enables you to use a local MTA in the host machine to forward mail using

    SOGoMailingMechanism = "smtp";
    SOGoSMTPServer = "GATEWAY";
 
For further details in MTA configuration including SMTP auth, refer to SOGo's documentation.

### Apache and HTTPs

As already given above, the default Apache configuration is already available under `etc/apache-SOGo.conf.orig`. The container exposes HTTP (80), HTTPS (443) and 8800, which is used by Apple devices, and 20000, the default port the SOGo daemon listens on. You can either directly include the certificates within the container, or use an external proxy for this. Make sure to only map the required ports to not unnecessarily expose daemons.

You would then need to adjust the `<Proxy ...>` section and include port, server name and url to match your setup.

    <Proxy http://127.0.0.1:20000/SOGo>
    ## adjust the following to your configuration
      RequestHeader set "x-webobjects-server-port" "443"
      RequestHeader set "x-webobjects-server-name" "sogo.example.net"
      RequestHeader set "x-webobjects-server-url" "https://sogo.example.net"

If you want to support iOS-devices, add appropriate `.well-known`-rewrites in either the Apache configuration or an external proxy.

ActiveSync is not included in this image for licencing reasons.

### Cron-Jobs: Backup, Session Timeout, Sieve

SOGo heavily relies on cron jobs. The image provides SOGo's original cron file as `./etc/cron.orig`. Copy and edit it as `./etc/cron`. The backup script is available and made executable at the predefined location `/usr/share/doc/sogo/sogo-backup.sh`, so backup is fully functional immediately after uncommenting the respective cron job.

### Further Configuration

Unlike the Debian and probably other SOGo packages, the number of worker processes is not set in `/etc/default/sogo`, but the normal `sogo.conf`. Remember to start a reasonable number of worker processes matching to your needs (8 will not be enough for medium and larger instances):

    WOWorkersCount = 8;

## Running a Container

Run the image in a container, expose ports as needed and making `/srv` permanent. An example run command, which links to a database container named `db` and uses an external HTTP proxy for wrapping in HTTPS might be

    docker run -d \
      --name='sogo' \
      --publish='127.0.0.1:80:80' \
      --link='sogo-postgresql:db' \
      --volume='/srv/sogo:/srv' \
      sebtombs/sogo3

## Upgrading and Maintenance

Read the _Upgrading_ section of the [Installation Guide](http://www.sogo.nu/files/docs/SOGo%20Installation%20Guide.pdf) prior to upgrading the container to verify whether anything special needs to be considered.

## IMPORTANT

This software is provided on the basis that it may be useful but has no warranty of any kind. Please see the [LICENCE](https://bitbucket.org/sebtombs/sogo3-docker/src/master/LICENSE) file for full details.
